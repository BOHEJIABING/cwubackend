package com.wpp.school.config;

import com.wpp.school.interceptor.InterceptorParam;
import com.wpp.school.interceptor.InterceptorTokenUser;
import org.redisson.Redisson;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * FileName: WebMvcConfig
 * Author:   lvyabin
 * Date:     2018/11/26 11:00 PM
 * Description: 配置拦截器和拦截的资源
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Autowired
    private InterceptorParam interceptorParam;

    @Autowired
    private InterceptorTokenUser interceptorTokenUser;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //不拦截的静态资源
        String[] resource = {"/css/**", "/js/**", "/lib/**", "/images/**", "/to/**", "/fonts/**", "/static/**"};
        String[] resourceUserToken = {"/ds1/user/updateMoiveType","/ds1/user/updateVovi","/ds1/user/updateVoviProv","/ds1/user/updateVoviType",
                "/ds1/user/getUUID","/ds1/user/login","/ds1/user/getVoviTypeInfo","/ds1/user/getVrTypeInfo","/ds1/user/getPolicyType","/ds1/user/register"};
        //拦截所有请求，排除resource数组中的资源
        registry.addInterceptor(interceptorParam).addPathPatterns("/**").excludePathPatterns(resource);
        registry.addInterceptor(interceptorTokenUser)
                .addPathPatterns("/ds1/user/**","/ds1/app/**")
                .excludePathPatterns(resourceUserToken);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //将所有/static/** 访问都映射到classpath:/static/ 目录下，避免访问不到
        registry.addResourceHandler("/admin/**").addResourceLocations("classpath:/templates/");
//        registry.addResourceHandler("/").addResourceLocations("classpath:/templates/index.html");
    }

    // 日期转换器  string --> date
    @Bean
    public Converter<String, Date> addNewConvert() {
        return new Converter<String, Date>() {
            public Date convert(String source) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = null;
                try {
                    date = sdf.parse(source);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return date;
            }
        };
    }

    // string[] --> Integer[]  str的格式为  1,4,4,2
    @Bean
    public Converter<String, Integer[]> strArray2IntArray() {
        return new Converter<String, Integer[]>() {
            public Integer[] convert(String source) {
                String[] split = source.split(",");
                int length = split.length;
                Integer[] ints = new Integer[length];
                for (int i = 0; i < length; i++) {
                    ints[i] = Integer.valueOf(split[i]);
                }
                return ints;
            }
        };
    }

    // string --> Integer
    @Bean
    public Converter<String, Integer> str2Integer() {
        return new Converter<String, Integer>() {
            public Integer convert(String source) {
                if (source==null||source.trim().equals(""))
                    return null;
                else if (source.equals("null") || source.equals("undefined"))
                    return null;
                source = source.trim();
                return Integer.valueOf(source);
            }
        };
    }

    // string转化为数字数组  str的格式为  1,4,4,2
    @Bean
    public Converter<String, String[]> str2StringArray() {
        return new Converter<String, String[]>() {
            public String[] convert(String source) {
                String[] split = source.split(",");
                int length = split.length;
                String[] strs = new String[length];
                for (int i = 0; i < length; i++) {
                    strs[i] = split[i];
                }
                return strs;
            }
        };
    }


    // string-->string   trim
    @Bean
    public Converter<String, String> str2String() {
        return new Converter<String, String>() {
            public String convert(String source) {
                if (source==null||source.trim().equals(""))
                    return null;
                else
                    return source;
            }
        };
    }

    // 全局跨域支持
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // 设置允许跨域的路由
        registry.addMapping("/**")
                // 设置允许跨域请求的域名
                .allowedOriginPatterns("*")
                // 是否允许证书（cookies）
                .allowCredentials(true)
                // 设置允许的⽅法
                .allowedMethods("*")
                // 跨域允许时间
                .maxAge(3600);
    }
    /**
     * 注册一个restTemplate到spring容器中
     * @return
     */
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    public Redisson redisson(){
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://192.168.77.128:6379")
                .setDatabase(0)
                .setPassword("123456");
        return (Redisson) Redisson.create(config);
    }


}

/**
 * 〈配置拦截器和拦截的资源〉
 *
 * @author lvyabin
 * @create 2018/11/26
 * @since 1.0.0
 */


