package com.wpp.school.config;

/**
 * ClassName: Json
 * Description:
 * date: 2022/3/31 21:36
 *
 * @author BOHE
 * @since JDK 1.8
 */
public class Json {
    private String code;
    private String msg;
    private Object result;

    private Json() {

    }

    private Json(String code, String msg, Object result) {
        this.msg = msg;
        this.result = result;
        this.code = code;
    }

    /**
     * @lvyabin 成功结果
     */
    public static Json success(Object result) {
        return new Json("200", "操作成功！", result);
    }

    public static Json success(String msg, Object result) {
        return new Json("200", msg, result);
    }

    /**
     * @lvyabin 失败结果
     */
    public static Json fail(Result result) {
//        throw new PuyunException(result);
        return new Json(result.getResultCode(), result.getResultMsg(), null);
    }

    public static Json fail(String code, String msg) {
//        throw new PuyunException(code,msg);
        return new Json(code, msg, null);
    }

    public static Json fail(String msg) {
//        throw new PuyunException("-1",msg);
        return new Json("-1", msg, null);
    }

    /**
     * @lvyabin 自定义结果
     */
    public static Json rs(Result result, Object obj) {
        return new Json(result.getResultCode(), result.getResultMsg(), result);
    }

    public static Json rs(String code, String msg, Object obj) {
        return new Json(code, msg, obj);
    }

    public static Json rs(String msg, Object obj) {
        return new Json("0", msg, obj);
    }

    public static Json rs(String msg) {
        return new Json("0", msg, null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
