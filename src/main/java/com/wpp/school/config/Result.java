package com.wpp.school.config;


import com.wpp.school.exception.BaseErrorInfoInterface;

public enum Result implements BaseErrorInfoInterface {
    // 数据操作错误定义
    SUCCESS("200", "操作成功!"),
    BODY_NOT_MATCH("400", "请求的数据格式不符!"),
    SIGNATURE_NOT_MATCH("401", "请求的数字签名不匹配!"),
    NOT_FOUND("404", "未找到该资源!"),
    INTERNAL_SERVER_ERROR("500", "服务器内部错误!"),
    SERVER_BUSY("503", "服务器正忙，请稍后再试!"),
    NO_LOGIN("1001", "请先登录"),
    PARAM_ERROR("1002", "信息未填写完整"),
    INFO_NO_EXISTS("1003", "信息不存在"),
    ACTION_FAIL("1004", "操作失败"),
    UPLOAD_FAIL("1005", "文件上传失败"),
    DB_ERROR("1006", "数据库错误");

    /**
     * 错误码 0为正确 -1为未定义的错误
     */
    private String resultCode;

    /**
     * 错误描述
     */
    private String resultMsg;

    Result(String resultCode, String resultMsg) {
        this.resultCode = resultCode;
        this.resultMsg = resultMsg;
    }

    @Override
    public String getResultCode() {
        return resultCode;
    }

    @Override
    public String getResultMsg() {
        return resultMsg;
    }
}




