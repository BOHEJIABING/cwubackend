package com.wpp.school.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * ClassName: RedisConfig
 * Description:
 * date: 2021/6/30 10:23
 * 处理redis内的数据
 * @author BOHE
 * @since JDK 1.8
 */

@Configuration
public class RedisConfig {
    @Bean("qdRedisTemplate")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory
                                                               redisConnectionFactory){
        RedisTemplate<String,Object> template = new RedisTemplate<>();
        Jackson2JsonRedisSerializer<Object> j = new Jackson2JsonRedisSerializer<Object>
                (Object.class);
        //value序列化
        template.setValueSerializer(j);
        template.setHashValueSerializer(j);
        //key序列化
        template.setKeySerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setConnectionFactory(redisConnectionFactory);
        return template;
    }
}