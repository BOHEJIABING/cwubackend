/**
 * FileName: InterceptorParam
 * Author:   lvyabin
 * Date:     2018/11/26 10:41 PM
 * Description: 控制台显示所有的请求和参数
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.wpp.school.interceptor;

import com.wpp.school.common.JSONChange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 〈控制台显示所有的请求和参数〉
 *
 * @author lvyabin
 * @create 2018/11/26
 * @since 1.0.0
 */
@Component
public class InterceptorParam implements HandlerInterceptor {
    private static final Logger log = LoggerFactory.getLogger(InterceptorParam.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("方法:" + request.getRequestURI());
        log.info("参数开始:");
        Map<String, String[]> parameterMap = request.getParameterMap();
        String s = JSONChange.objToJson(parameterMap);
        log.info(s.replace("\"", ""));
        log.info("参数结束:");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {

    }
}
