/**
 * FileName: InterceptorParam
 * Author:   lvyabin
 * Date:     2018/11/26 10:41 PM
 * Description: 控制台显示所有的请求和参数
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.wpp.school.interceptor;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wpp.school.config.Result;
import com.wpp.school.ds1.entity.TUser;
import com.wpp.school.ds1.service.IUserService;
import com.wpp.school.exception.PuyunException;
import org.apache.catalina.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 〈控制台显示所有的请求和参数〉
 *
 * @author lvyabin
 * @create 2018/11/26
 * @since 1.0.0
 */
@Component
public class InterceptorTokenUser implements HandlerInterceptor {
    private static final Logger log = LoggerFactory.getLogger(InterceptorTokenUser.class);
    @Autowired
    private IUserService userService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//		String token = request.getParameter("adminToken");
        String token = request.getHeader("token");
        if (token == null)
            throw new PuyunException(Result.NO_LOGIN);
        TUser user = userService.getOne(new QueryWrapper<TUser>().eq("token", token));
        if (user == null)
            throw new PuyunException(Result.NO_LOGIN);
//		LocalDateTime endTime = user.getEndTime();
//		LocalDateTime now = LocalDateTime.now();
//		user.setFinalTime(LocalDateTime.now());
//		user.setFinalIp(PYHttpUtils.getRealIp(request));
        userService.updateById(user);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {

    }
}
