package com.wpp.school.ds1.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wpp.school.config.Json;
import com.wpp.school.config.Result;
import com.wpp.school.ds1.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: TimetableController
 * Description:
 * date: 2022/4/1 23:10
 *
 * @author BOHE
 * @since JDK 1.8
 */
@RestController
@RequestMapping("/timetable")
public class TimetableController extends BaseController {

    @Autowired
    TTimetable tTimetable;

    /******************************************************平板端***********************************************/

    /**
     * 根据层级id获取课表
     *
     * @param conditionId 层级id
     * @return 课表列表（baseImg）
     */
    @PostMapping("/getTimetableByCId")
    public Json getTimetableByCId(Integer conditionId) {
        if (conditionId == null) {
            conditionId = 1;
        }
        List<TTimetable> tTimetables = timetableService.list(getBaseQuery().eq("condition_id", conditionId));
        if (tTimetables.isEmpty()) {
            return Json.fail(Result.NOT_FOUND);
        }
        ArrayList<BaseImg> timetableList = new ArrayList<>();
        for (TTimetable tTimetable : tTimetables) {
            BaseImg baseImg = new BaseImg(tTimetable.getTimetableUrl(), tTimetable.getTimetableUrlSelected());
            timetableList.add(baseImg);
        }
        return Json.success(timetableList);
    }


    /******************************************************管理端***********************************************/
    /**
     * 根据层级id获取课表
     *
     * @param conditionId 层级id
     * @return 课表列表（timetableWeb）
     */
    @PostMapping("/getTimetableByCIdWeb")
    public Json getTimetableByCIdWeb(Integer conditionId) {
        // 如果没有指定有效的conditionId，则从map中取值
        if (conditionId == null || conditionId == 0) {
            if (!map.isEmpty()) {
                conditionId = (Integer) map.get("firstConditionId");
            } else {
                conditionId = 1;
            }
        }
        List<TTimetable> tTimetables = timetableService.list(new QueryWrapper<TTimetable>()
                .select("timetable_url", "timetable_url_selected", "id", "day_of_week", "condition_id")
                .eq("condition_id", conditionId)
                .eq("status", STATUS_ACTIVE)
                .orderByAsc("day_of_week"));
        TCondition condition = conditionService.getById(conditionId);
        ArrayList<TimetableWeb> timetableList = new ArrayList<>();
        for (TTimetable time : tTimetables) {
            TimetableWeb timetableWeb = new TimetableWeb(time.getId(), time.getTimetableUrl(),
                    time.getTimetableUrlSelected(), time.getDayOfWeek(), condition.getSecondCondition(), time.getStatus());
            timetableList.add(timetableWeb);
        }
        return Json.success(timetableList);
    }


    /**
     * 添加课表
     *
     * @param timetableFromWeb conditionId: 课表所属层级，timetableUrlList: 未选中课表图片列表，timetableUrlSelected: 选中课表图片列表
     * @return 是否添加成功
     */
    @PostMapping("/addTimetableList")
    public Json addTimetableList(@RequestBody TimetableFromWeb timetableFromWeb) {
        checkUserPermission(null);
        List<String> timetableSelectUrlList = timetableFromWeb.getTimetableSelectUrlList();
        List<String> timetableUrlList = timetableFromWeb.getTimetableUrlList();
        int conditionId = Integer.parseInt(timetableFromWeb.getConditionId());

        TCondition byId = conditionService.getById(conditionId);
        if (byId == null) {
            return Json.fail("层级不存在，请刷新后重试！");
        }
        // 检查列表内容是否完整
        for (int i = 0; i < timetableSelectUrlList.size(); i++) {
            if (timetableSelectUrlList.get(i) == null || timetableUrlList.get(i) == null) {
                return Json.fail("图片未选择完整！");
            }
        }
        // 添加课表
        for (int i = 0; i < timetableSelectUrlList.size(); i++) {
            tTimetable.setDayOfWeek(i + 1);
            tTimetable.setTimetableUrl(timetableUrlList.get(i));
            tTimetable.setTimetableUrlSelected(timetableSelectUrlList.get(i));
            tTimetable.setConditionId(conditionId);
            boolean save = timetableService.save(tTimetable);
            if (!save) {
                return Json.fail("添加失败，请稍后重试！");
            }
        }
        return Json.success("添加课表成功！");
    }

    /**
     * 修改课表
     *
     * @param id      课表id
     * @param image   课表图片
     * @param imgType 修改的是选中的课表还是未选中的课表
     * @return 是否修改成功
     */
    @PostMapping("/editTimetable")
    public Json editTimetable(Integer id, String image, String imgType) {
        checkUserPermission(null);
        TTimetable tTimetable = timetableService.getById(id);
        if (tTimetable == null) {
            return Json.fail("课表不存在！");
        }
        if (imgType.equals("timetable")) {
            tTimetable.setTimetableUrl(image);
        } else {
            tTimetable.setTimetableUrlSelected(image);
        }
        boolean save = timetableService.updateById(tTimetable);
        return actionIsFail(save);
    }

    /******************************************************通用***********************************************/

    public QueryWrapper<TTimetable> getBaseQuery() {
        return new QueryWrapper<TTimetable>().select("timetable_url", "timetable_url_selected")
                .eq("status", STATUS_ACTIVE)
                .orderByAsc("day_of_week");
    }

}
