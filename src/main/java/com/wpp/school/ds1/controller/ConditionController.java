package com.wpp.school.ds1.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wpp.school.common.HandleCascader;
import com.wpp.school.config.Json;
import com.wpp.school.config.Result;
import com.wpp.school.ds1.entity.BaseConfig;
import com.wpp.school.ds1.entity.ConditionCascader;
import com.wpp.school.ds1.entity.TCondition;
import com.wpp.school.exception.PuyunException;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: ConditionController
 * Description: 层级controller
 * date: 2022/3/31 22:34
 *
 * @author BOHE
 * @since JDK 1.8
 */

@RestController
@RequestMapping("/condition")
public class ConditionController extends BaseController {

    @Autowired
    TCondition condition;

    /******************************************************展示端***********************************************/

    // 获取所有层级
    @PostMapping("/getAllCondition")
    public Json getAllCondition() {
        return getConditionByModule();
    }

    // 根据模块获取层级，只返回有课表的层级
    @NotNull
    public Json getConditionByModule() {
        List<TCondition> tConditionList;
        tConditionList = conditionService.list(getBaseQuery().eq("have_timetable", HAVE_TIMETABLE));
        if (tConditionList.isEmpty()) {
            return Json.fail(Result.NOT_FOUND);
        }
        ArrayList<BaseConfig> conditions = new ArrayList<>();
        for (TCondition tcondition : tConditionList) {
            BaseConfig baseConfig = new BaseConfig(tcondition.getFirstCondition(), tcondition.getSecondCondition(),
                    tcondition.getId(), tcondition.getModule());
            conditions.add(baseConfig);
        }
        return Json.success(conditions);
    }

    /******************************************************管理端***********************************************/

    /**
     * 获取层级列表
     *
     * @return 层级列表
     */
    @PostMapping("/getConditionList")
    public Json getConditionList() {
        List<TCondition> conditionList = conditionService.list(getBaseQuery());
        if (conditionList.size() == 0) {
            return Json.fail(Result.NOT_FOUND);
        }
        // 存第一个的层级id，用于未选择时展示
        map.put("firstConditionId", conditionList.get(0).getId());
        try {
            List<ConditionCascader> conditionCascaders = HandleCascader.Companion.handleList(conditionList);
            if (conditionCascaders.size() == 0) {
                return Json.fail("当前没有层级，请添加！");
            }
            return Json.success(conditionCascaders);
        } catch (Exception e) {
            Json.fail("层级加载失败，请稍后重试！");
        }
        return Json.fail("层级加载失败，请稍后重试！");
    }


    /**
     * 编辑层级
     *
     * @param id              被修改的层级id
     * @param firstCondition  第一层级
     * @param secondCondition 第二层级
     * @param status          层级状态
     * @param isSyn           是否同步（第一层级修改）
     * @return 是否修改成功
     */
    @PostMapping("/editCondition")
    public Json editCondition(Integer id, String firstCondition, String secondCondition, Integer status, Boolean isSyn) {
        checkUserPermission(null);
        checkIsConditionExist(firstCondition, secondCondition);
        // 如果没有指定有效的conditionId，则从map中取值
        if (id == null || id == 0) {
            if (!map.isEmpty()) {
                id = (Integer) map.get("firstConditionId");
                System.out.println("---map conditionId: " + id);
            } else {
                id = 1;
            }
        }
        TCondition condition = conditionService.getById(id);
        if (condition == null) {
            return Json.fail("层级不存在！");
        }
        // 修改第二层级
        if (secondCondition != null && !secondCondition.isEmpty()) {
            checkIsConditionExist(condition.getFirstCondition(), secondCondition);
            condition.setSecondCondition(secondCondition);
        }
        // 修改状态
        if (status != null) {
            condition.setStatus(status);
        }
        // 修改第一层级
        if (firstCondition != null && !firstCondition.isEmpty()) {
            checkIsConditionExist(firstCondition, condition.getSecondCondition());
            String fCondition = condition.getFirstCondition();
            condition.setFirstCondition(firstCondition);
            boolean save = conditionService.updateById(condition);
            if (!save) {
                return Json.fail("修改失败！");
            }
            if (isSyn) {
                // 同步修改所有第一层级
                editAllFirstCondition(fCondition, firstCondition);
            }
            return Json.success("修改成功！");
        }
        boolean save = conditionService.updateById(condition);
        return actionIsFail(save);
    }

    /**
     * 新增层级
     *
     * @param firstCondition  第一层级
     * @param secondCondition 第二层级
     * @param module          所属模块
     * @return 添加之后的层级id
     */
    @PostMapping("/addCondition")
    public Json addCondition(String firstCondition, String secondCondition, Integer module) {
        checkUserPermission(null);
        if (firstCondition == null || secondCondition == null || module == null) {
            return Json.fail(Result.PARAM_ERROR);
        }
        checkIsConditionExist(firstCondition, secondCondition);
        condition.setFirstCondition(firstCondition);
        condition.setModule(module);
        condition.setSecondCondition(secondCondition);
        boolean save = conditionService.save(condition);
        if (save) {
            TCondition one = conditionService.getOne(getWebQuery(firstCondition, secondCondition));
            return Json.success("操作成功", one.getId());
        }
        return Json.fail(Result.ACTION_FAIL);
    }

    /**
     * 根据层级内容判断该层级是否有课表
     *
     * @param firstCondition  第一层级内容
     * @param secondCondition 第二层级内容
     * @return 层级id，层级是否有课表
     */
    @PostMapping("/checkIsHaveTimetable")
    public Json checkIsHasTimetable(String firstCondition, String secondCondition) {
        checkStringIsNull(firstCondition, secondCondition);
        TCondition conditionById = conditionService.getOne(getWebQuery(firstCondition, secondCondition).select("have_timetable", "id"));
        if (conditionById == null) {
            return Json.fail(Result.NOT_FOUND);
        }
        System.out.println(conditionById.toString());
        Map<String, Object> map = new HashMap<>();
        map.put("conditionId", conditionById.getId());
        map.put("isHaveTimetable", conditionById.getHaveTimetable());
        return Json.success(map);
    }

    /**
     * 修改所有第一层级
     *
     * @param originFirstCondition 原来的第一层级
     * @param editFirstCondition   需改后的第一层级
     */
    private void editAllFirstCondition(String originFirstCondition, String editFirstCondition) {
        List<TCondition> tConditions = conditionService.list(new QueryWrapper<TCondition>()
                .eq("first_condition", originFirstCondition).eq("status", STATUS_ACTIVE));
        for (TCondition condition : tConditions) {
            // 保证不重复
            checkIsConditionExist(editFirstCondition, condition.getSecondCondition());
            condition.setFirstCondition(editFirstCondition);
            boolean save = conditionService.updateById(condition);
            if (!save) {
                throw new PuyunException("保存失败，请重试！");
            }
        }
    }

    /******************************************************通用***********************************************/

    /**
     * 判断层级是否已存在
     *
     * @param firstCondition  第一层级
     * @param secondCondition 第二层级
     */
    private void checkIsConditionExist(String firstCondition, String secondCondition) {
        if (firstCondition != null && secondCondition != null) {
            TCondition conditionById = conditionService.getOne(getWebQuery(firstCondition, secondCondition));
            if (conditionById != null) {
                throw new PuyunException("该层级已存在！");
            }
        }
    }



    /**
     * 获取web端查询的BaseQuery
     *
     * @param firstCondition  第一层级
     * @param secondCondition 第二层级
     * @return web查询常用的query
     */
    private QueryWrapper<TCondition> getWebQuery(String firstCondition, String secondCondition) {
        return new QueryWrapper<TCondition>().select("id")
                .eq("first_condition", firstCondition)
                .eq("second_condition", secondCondition)
                .eq("status", STATUS_ACTIVE);
    }

    /**
     * 获取平板端展示用的BaseQuery
     *
     * @return 平板端展示用的Query
     */
    private QueryWrapper<TCondition> getBaseQuery() {
        return new QueryWrapper<TCondition>()
                .select("id", "module", "first_condition", "second_condition")
                .eq("status", STATUS_ACTIVE)
                .orderByAsc("module", "first_condition");
    }
}
