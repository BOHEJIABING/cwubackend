package com.wpp.school.ds1.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wpp.school.config.Json;
import com.wpp.school.config.Result;
import com.wpp.school.ds1.entity.BaseImg;
import com.wpp.school.ds1.entity.TShowImg;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * ClassName: ShowImgController
 * Description:
 * date: 2022/4/1 15:09
 *
 * @author BOHE
 * @since JDK 1.8
 */
@RestController
@RequestMapping("/showImg")
public class ShowImgController extends BaseController {

    @Autowired
    private TShowImg showImg;

    /******************************************************展示端***********************************************/

    // 获取推荐列表
    @PostMapping("/getMainRecommend")
    public Json getMainRecommend() {
        return getModuleBaseImgList(MAIN_RECOMMEND);
    }

    // 获取新闻动态
    @PostMapping("/getNews")
    public Json getNews() {
        return getModuleBaseImgList(NEWS);
    }

    // 获取教学实践
    @PostMapping("/getPractice")
    public Json getPractice() {
        return getModuleBaseImgList(PRACTICE);
    }

    // 获取学生活动
    @PostMapping("/getEnjoyStu")
    public Json getEnjoyStu() {
        return getModuleBaseImgList(STU_ENJOY);
    }

    // 获取教师活动
    @PostMapping("/getEnjoyTeacher")
    public Json getEnjoyTeacher() {
        return getModuleBaseImgList(TEACH_ENJOY);
    }

    // 获取指定模块的所有baseImg
    @NotNull
    private Json getModuleBaseImgList(Integer module) {
        List<TShowImg> tShowImgList = showImgService.list(getBaseQuery(module));
        if (tShowImgList.isEmpty()) {
            return Json.fail(Result.NOT_FOUND);
        }
        ArrayList<BaseImg> moduleList = new ArrayList<>();
        for (TShowImg tShowImg : tShowImgList) {
            moduleList.add(new BaseImg(tShowImg.getBaseUrl(), tShowImg.getDetailUrl()));
        }
        return Json.success(moduleList);
    }


    /******************************************************管理端***********************************************/

    /**
     * 添加TShowImg到指定模块
     *
     * @param baseUrl   base链接
     * @param detailUrl detail链接
     * @param module    所属模块
     * @return 添加是否成功
     */
    @PostMapping("/addImg")
    public Json addImg(String baseUrl, String detailUrl, Integer module) {
        checkUserPermission(null);
        if (baseUrl == null || detailUrl == null || module == null) {
            return Json.fail(Result.PARAM_ERROR);
        }
        showImg.setBaseUrl(baseUrl);
        showImg.setDetailUrl(detailUrl);
        showImg.setModule(module);
        boolean save = showImgService.save(showImg);
        if (save) {
            return Json.success(Result.SUCCESS);
        }
        return Json.fail(Result.ACTION_FAIL);
    }

    /**
     * 修改TShowImg的内容
     *
     * @param id   showImg的id
     * @param img  修改的具体内容
     * @param type 修改的是base还是detail
     * @return 是否修改成功
     */
    @PostMapping("/editImgContent")
    public Json editImgContent(Integer id, String img, String type) {
        checkUserPermission(null);
        TShowImg showImg = showImgService.getById(id);
        if (showImg == null) {
            return Json.fail("图片组不存在！");
        }
        if (type.equals("base")) {
            showImg.setBaseUrl(img);
        } else {
            showImg.setDetailUrl(img);
        }

        boolean updateById = showImgService.updateById(showImg);
        return actionIsFail(updateById);
    }

    /**
     * 修改showImg的状态
     *
     * @param id     showImg的id
     * @param status 修改的状态
     * @return 是否修改成功
     */
    @PostMapping("/editImgStatus")
    public Json editImgStatus(Integer id, Integer status) {
        checkUserPermission(null);
        TShowImg showImg = showImgService.getById(id);
        if (showImg == null) {
            return Json.fail("图片组不存在！");
        }
        showImg.setStatus(status);
        boolean updateById = showImgService.updateById(showImg);
        return actionIsFail(updateById);
    }

    /**
     * 获取指定模块的showImg列表, 参数为空时请求所有的showImg
     *
     * @param module 层级所属模块
     * @return 该模块列表
     */
    @PostMapping("/getImgList")
    private Json getImgList(Integer module) {
        List<TShowImg> tShowImgList = showImgService.list(getAllQuery(module));
        if (tShowImgList.isEmpty()) {
            return Json.fail(Result.NOT_FOUND);
        }
        return Json.success(tShowImgList);
    }

    /******************************************************通用***********************************************/

    private QueryWrapper<TShowImg> getAllQuery(Integer module) {
        QueryWrapper<TShowImg> baseQuery = new QueryWrapper<TShowImg>().select("id", "base_url", "detail_url", "module", "status").orderByAsc("module");
        // 指定模块
        if (module != null) {
            baseQuery = baseQuery.eq("module", module);
            return baseQuery;
        }
        // 所有模块
        return baseQuery;
    }

    private QueryWrapper<TShowImg> getBaseQuery(Integer module) {
        return new QueryWrapper<TShowImg>().select("base_url", "detail_url")
                .eq("status", STATUS_ACTIVE).eq("module", module);
    }

}
