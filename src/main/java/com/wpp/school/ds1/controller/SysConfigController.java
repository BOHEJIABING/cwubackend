package com.wpp.school.ds1.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wpp.school.config.Json;
import com.wpp.school.config.Result;
import com.wpp.school.ds1.entity.BaseConfig;
import com.wpp.school.ds1.entity.TSysConfig;
import com.wpp.school.exception.PuyunException;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * ClassName: SysConfigController
 * Description:
 * date: 2022/4/1 23:10
 *
 * @author BOHE
 * @since JDK 1.8
 */
@RestController
@RequestMapping("/config")
public class SysConfigController extends BaseController {

    /******************************************************展示端***********************************************/

    // 获取enjoy主模块背景
    @PostMapping("/getEnjoyBackground")
    public Json getEnjoyBackground() {
        return getBaseConfig("enjoy_background", ENJOY);
    }

    // 获取学院简介
    @PostMapping("/getIntroduce")
    public Json getIntroduce() {
        return getBaseConfig("introduce", INTRODUCE);
    }

    // 获取无操作返回主页时长
    @PostMapping("/getTimeToHome")
    public Json getTimeToHome() {
        try {
            BaseConfig baseConfig = (BaseConfig) getBaseConfig("time_to_home", MAIN_RECOMMEND).getResult();
            return Json.success(baseConfig.getMainConfig());
        } catch (Exception e) {
            return Json.fail("获取失败，请联系管理员！");
        }
    }

    // 获取轮播图轮播时间
    @PostMapping("/getLoopTime")
    public Json getLoopTime() {
        try {
            BaseConfig baseConfig = (BaseConfig) getBaseConfig("loop_time", MAIN_RECOMMEND).getResult();
            return Json.success(baseConfig.getMainConfig());
        } catch (Exception e) {
            return Json.fail("获取失败，请联系管理员！");
        }
    }


    @NotNull
    private Json getBaseConfig(String name, Integer module) {
        if (name == null || module == null) {
            throw new PuyunException(Result.PARAM_ERROR);
        }
        try {
            TSysConfig sysConfig = sysConfigService.getOne(getBaseQuery().eq("name", name).eq("module", module));
            if (sysConfig == null) {
                throw new PuyunException(Result.NOT_FOUND);
            }
            return Json.success(new BaseConfig(sysConfig.getConfigMain(), sysConfig.getConfigSpare()));
        } catch (Exception e) {
            return Json.fail("获取失败，请联系管理员！");
        }
    }

    public QueryWrapper<TSysConfig> getBaseQuery() {
        return new QueryWrapper<TSysConfig>().select("config_main", "config_spare")
                .eq("status", STATUS_ACTIVE);
    }

    /******************************************************管理端***********************************************/

    /**
     * 根据状态获取设置
     *
     * @param status 状态
     * @return 设置列表
     */
    @PostMapping("/getConfigList")
    public Json getConfigList(Integer status) {
        List<TSysConfig> listByStatus = getListByStatus(status);
        if (listByStatus.size() == 0) {
            return Json.fail("无处于该状态的设置！");
        }
        return Json.success(listByStatus);
    }

    /**
     * 更改设置状态
     *
     * @param id     设置id
     * @param status 设置状态
     * @return 是否操作成功
     */
    @PostMapping("/editConfigStatus")
    public Json editConfigStatus(Integer id, Integer status) {
        checkUserPermission(null);
        TSysConfig sysConfig = sysConfigService.getById(id);
        if (sysConfig == null) {
            return Json.fail("设置不存在！");
        }
        sysConfig.setStatus(status);
        boolean updateById = sysConfigService.updateById(sysConfig);
        return actionIsFail(updateById);
    }

    /**
     * 修改设置内容
     *
     * @param id          设置id
     * @param mainConfig  主要设置内容
     * @param spareConfig 备用设置内容
     * @return 是否操作成功
     */
    @PostMapping("/editConfigContent")
    public Json editConfigContent(Integer id, String mainConfig, String spareConfig) {
        checkUserPermission(null);
        TSysConfig sysConfig = sysConfigService.getById(id);
        if (sysConfig == null) {
            return Json.fail("设置不存在！");
        }
        if (mainConfig != null) {
            sysConfig.setConfigMain(mainConfig);
        }
        if (spareConfig != null) {
            sysConfig.setConfigSpare(spareConfig);
        }
        boolean b = sysConfigService.updateById(sysConfig);
        return actionIsFail(b);
    }

    // 根据状态获取list
    private List<TSysConfig> getListByStatus(Integer status) {
        QueryWrapper<TSysConfig> query = new QueryWrapper<>();
        if (status == null) {
            return sysConfigService.list(query);
        }
        return sysConfigService.list(query.eq("status", status));
    }

}
