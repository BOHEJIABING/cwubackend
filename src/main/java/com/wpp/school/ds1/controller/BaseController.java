package com.wpp.school.ds1.controller;

import com.wpp.school.config.Json;
import com.wpp.school.config.Result;
import com.wpp.school.ds1.entity.TUser;
import com.wpp.school.ds1.service.*;
import com.wpp.school.exception.PuyunException;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: BaseController
 * Description: controller基类
 * date: 2022/4/1 15:25
 *
 * @author BOHE
 * @since JDK 1.8
 */
public class BaseController {

    @Autowired
    protected IUserService userService;

    @Autowired
    protected IConditionService conditionService;

    @Autowired
    protected IShowImgService showImgService;

    @Autowired
    protected ITimetableService timetableService;

    @Autowired
    protected ISysConfigService sysConfigService;

    // 设备
    static final String DEVICE_TABLE = "device_table";
    static final String DEVICE_WEB = "device_web";

    // 首页
    static final Integer MAIN_RECOMMEND = 0;
    static final Integer MAIN_BACKGROUND = 100;

    // 学院简介
    static final Integer INTRODUCE = 1;

    // 新闻动态
    static final Integer NEWS = 2;

    // 教学实践
    static final Integer PRACTICE = 3;

    // 教学安排
    static final Integer TIMETABLE = 4;
    static final Integer STU_TIMETABLE = 40;
    static final Integer TEACH_TIMETABLE = 41;
    static final Integer ROOM_TIMETABLE = 42;

    // 师生活动
    static final Integer ENJOY = 5;
    static final Integer STU_ENJOY = 50;
    static final Integer TEACH_ENJOY = 51;

    // status状态码
    static final Integer STATUS_ACTIVE = 1;
    static final Integer STATUS_INACTIVE = 0;
    static final Integer STATUS_DISABLE = -1;

    // user permission
    static final Integer PERMISSION_SUPER = 0;
    static final Integer PERMISSION_ONE = 1;
    static final Integer PERMISSION_TWO = 2;
    static final Integer PERMISSION_THREE = 3;

    // has timetable
    static final Integer NOT_HAVE_TIMETABLE = 0;
    static final Integer HAVE_TIMETABLE = 1;


    @Qualifier("qdRedisTemplate")
    @Autowired
    public RedisTemplate qdrt;

    protected Map<String, Object> map = new HashMap<>();

    protected TUser getUserByToken() {
        String token = getReq().getHeader("token");
        if (token == null)
            throw new PuyunException(Result.NO_LOGIN);
        Object idobj = qdrt.opsForValue().get(token);
        if (idobj == null)
            throw new PuyunException(Result.NO_LOGIN);
        Integer userId = Integer.valueOf(idobj.toString());
        TUser user = userService.getById(userId);
        if (user == null)
            throw new PuyunException(Result.NO_LOGIN);
        return user;
    }

    // 检查用户当前登录的管理员权限
    protected void checkUserPermission(Integer permission) {
        TUser user = getUserByToken();
        if (user.getPermissions() > PERMISSION_TWO) {
            throw new PuyunException("当前没有编辑权限！");
        }
        // 修改权限，判断权限是否大于(数值上小于)要修改管理员的权限
        if (permission != null && user.getPermissions() >= permission) {
            throw new PuyunException("管理员权限等级不够！");
        }
    }

    protected HttpServletRequest getReq() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return servletRequestAttributes.getRequest();
    }

    protected void checkStringIsNull(@NotNull String... args) {
        for (String arg : args) {
            if (arg == null || arg.isEmpty()) {
                throw new PuyunException(Result.PARAM_ERROR);
            }
        }
    }

    protected Json actionIsFail(@NotNull Boolean isSuccess) {
        if (isSuccess) {
            return Json.success(Result.SUCCESS);
        }
        return Json.fail(Result.ACTION_FAIL);
    }
}
