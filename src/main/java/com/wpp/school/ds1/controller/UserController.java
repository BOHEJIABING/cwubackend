package com.wpp.school.ds1.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wpp.school.common.MD5Util;
import com.wpp.school.common.RandomUtils;
import com.wpp.school.config.Json;
import com.wpp.school.config.Result;
import com.wpp.school.ds1.entity.TUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * ClassName: UserController
 * Description:
 * date: 2022/4/1 23:06
 *
 * @author BOHE
 * @since JDK 1.8
 */
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    private TUser tUser;

    // 获取所有管理员
    @PostMapping("/getUserList")
    public Json getUserList(Integer permissions) {
        List<TUser> users = getUserByPermission(permissions);
        if (users.size() <= 0) {
            return Json.fail("当前没有管理员！");
        }
        return Json.success(users);
    }

    // 修改管理员状态
    @PostMapping("/editUserStatus")
    public Json editUserStatus(Integer id, Integer status) {
        checkUserPermission(null);
        TUser userById = userService.getById(id);
        if (userById == null) {
            return Json.fail("用户不存在！");
        }
        userById.setStatus(status);
        boolean updateById = userService.updateById(userById);
        return actionIsFail(updateById);
    }

    // 修改管理员权限
    @PostMapping("/editUserPermissions")
    public Json editUserPermissions(Integer id, Integer permissions) {
        TUser userById = userService.getById(id);
        if (userById == null) {
            return Json.fail("用户不存在！");
        }
        checkUserPermission(userById.getPermissions());
        userById.setPermissions(permissions);
        boolean updateById = userService.updateById(userById);
        return actionIsFail(updateById);
    }

    // 注册
    @PostMapping("/register")
    public Json register(String name, String password, String schoolId) {
        checkStringIsNull(name, password, schoolId);
        if (isUserExisted(name, schoolId)) {
            return Json.fail("用户名或学号已注册！");
        }
        tUser.setPassword(MD5Util.getMD5Code(password, 100, schoolId));
        tUser.setUserName(name);
        tUser.setSchoolId(schoolId);
        return actionIsFail(userService.save(tUser));
    }

    // 登录
    @PostMapping("/login")
    public Json login(String schoolId, String password) {
        checkStringIsNull(password, schoolId);
        TUser user = findUserByColumn("school_id", schoolId);
        if (user == null) {
            return Json.fail("用户不存在！");
        }
        if (!user.getPassword().equals(MD5Util.getMD5Code(password, 100, schoolId))) {
            return Json.fail("密码错误");
        }

        String token = RandomUtils.getUUID();
        user.setToken(token);
        userService.updateById(user);
        //保存到redis中，设置30min过期
        qdrt.opsForValue().set(token, user.getId(), 30, TimeUnit.MINUTES);
        Map<String, Object> map = new HashMap<>();
        map.put("userName", user.getUserName());
        map.put("token", token);
        map.put("permissions", user.getPermissions());
        return Json.success("登录成功！", map);
    }


    private List<TUser> getUserByPermission(Integer permissions) {
        QueryWrapper<TUser> getUserQuery = new QueryWrapper<TUser>()
                .select("id", "user_name", "school_id", "permissions", "status")
                .orderByAsc("permissions");
        return (permissions == null) ? userService.list(getUserQuery) :
                userService.list(getUserQuery.eq("permissions", permissions));
    }

    private Boolean isUserExisted(String name, String schoolId) {
        return findUserByColumn("user_name", name) != null || findUserByColumn("school_id", schoolId) != null;
    }

    private TUser findUserByColumn(String column, String columnValue) {
        return userService.getOne(getBaseQuery().eq(column, columnValue));
    }

    private QueryWrapper<TUser> getBaseQuery() {
        return new QueryWrapper<TUser>().eq("status", STATUS_ACTIVE);
    }
}
