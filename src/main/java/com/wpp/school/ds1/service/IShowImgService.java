package com.wpp.school.ds1.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wpp.school.ds1.entity.TShowImg;

/**
 * ClassName: IShowImgService
 * Description:
 * date: 2022/4/1 15:26
 *
 * @author BOHE
 * @since JDK 1.8
 */
public interface IShowImgService extends IService<TShowImg> {
}
