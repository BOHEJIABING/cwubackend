package com.wpp.school.ds1.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wpp.school.ds1.entity.TCondition;
import com.wpp.school.ds1.mapper.ConditionMapper;
import com.wpp.school.ds1.service.IConditionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ClassName: ConditionServiceImpl
 * Description:
 * date: 2022/3/31 22:35
 *
 * @author BOHE
 * @since JDK 1.8
 */

@Service
public class ConditionServiceImpl extends ServiceImpl<ConditionMapper, TCondition> implements IConditionService {

    @Autowired
    private ConditionMapper conditionMapper;

    @Override
    public TCondition getConditionById(Integer conditionId) {
//         通过mapper获取数据库数据
        return conditionMapper.getConditionById(conditionId);
    }
}
