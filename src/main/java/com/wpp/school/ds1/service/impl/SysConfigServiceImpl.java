package com.wpp.school.ds1.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wpp.school.ds1.entity.TSysConfig;
import com.wpp.school.ds1.mapper.SysConfigMapper;
import com.wpp.school.ds1.service.ISysConfigService;
import org.springframework.stereotype.Service;

/**
 * ClassName: SysConfigServiceImpl
 * Description:
 * date: 2022/4/1 15:27
 *
 * @author BOHE
 * @since JDK 1.8
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, TSysConfig> implements ISysConfigService {
}
