package com.wpp.school.ds1.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wpp.school.ds1.entity.TUser;

/**
 * ClassName: IUserService
 * Description:
 * date: 2022/4/1 23:06
 *
 * @author BOHE
 * @since JDK 1.8
 */
public interface IUserService extends IService<TUser> {
}
