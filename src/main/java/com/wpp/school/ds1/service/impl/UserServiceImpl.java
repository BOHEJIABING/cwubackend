package com.wpp.school.ds1.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wpp.school.ds1.entity.TUser;
import com.wpp.school.ds1.mapper.UserMapper;
import com.wpp.school.ds1.service.IUserService;
import org.springframework.stereotype.Service;

/**
 * ClassName: UserServiceImpl
 * Description:
 * date: 2022/4/1 23:06
 *
 * @author BOHE
 * @since JDK 1.8
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, TUser> implements IUserService {
}
