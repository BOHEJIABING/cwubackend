package com.wpp.school.ds1.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wpp.school.ds1.entity.TSysConfig;

/**
 * ClassName: ISysConfigService
 * Description:
 * date: 2022/4/1 15:27
 *
 * @author BOHE
 * @since JDK 1.8
 */
public interface ISysConfigService extends IService<TSysConfig> {
}
