package com.wpp.school.ds1.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wpp.school.ds1.entity.TTimetable;
import com.wpp.school.ds1.mapper.TimetableMapper;
import com.wpp.school.ds1.service.ITimetableService;
import org.springframework.stereotype.Service;

/**
 * ClassName: TimetableServiceImpl
 * Description:
 * date: 2022/4/1 15:27
 *
 * @author BOHE
 * @since JDK 1.8
 */
@Service
public class TimetableServiceImpl extends ServiceImpl<TimetableMapper, TTimetable> implements ITimetableService {
}
