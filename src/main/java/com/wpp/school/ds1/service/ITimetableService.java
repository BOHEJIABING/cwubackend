package com.wpp.school.ds1.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wpp.school.ds1.entity.TTimetable;

/**
 * ClassName: ITimetableService
 * Description:
 * date: 2022/4/1 15:27
 *
 * @author BOHE
 * @since JDK 1.8
 */
public interface ITimetableService extends IService<TTimetable> {
}
