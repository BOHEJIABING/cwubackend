package com.wpp.school.ds1.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wpp.school.ds1.entity.TCondition;

/**
 * ClassName: ConditionService
 * Description:
 * date: 2022/3/31 22:35
 *
 * @author BOHE
 * @since JDK 1.8
 */
public interface IConditionService extends IService<TCondition> {

    TCondition getConditionById(Integer conditionId);
}
