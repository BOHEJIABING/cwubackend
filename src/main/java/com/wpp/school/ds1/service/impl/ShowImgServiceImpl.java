package com.wpp.school.ds1.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wpp.school.ds1.entity.TShowImg;
import com.wpp.school.ds1.mapper.ShowImgMapper;
import com.wpp.school.ds1.service.IShowImgService;
import org.springframework.stereotype.Service;

/**
 * ClassName: ShowImgServiceImpl
 * Description:
 * date: 2022/4/1 15:26
 *
 * @author BOHE
 * @since JDK 1.8
 */
@Service
public class ShowImgServiceImpl extends ServiceImpl<ShowImgMapper, TShowImg> implements IShowImgService {
}
