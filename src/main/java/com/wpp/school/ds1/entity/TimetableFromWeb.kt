package com.wpp.school.ds1.entity

/**
 *
 * ClassName: TimetableFromWeb
 * Description:
 * date: 2022/4/20/0020 16:43
 * @author BOHE
 * @version
 * @since JDK 1.8
 */
data class TimetableFromWeb(val conditionId: String, val timetableSelectUrlList: List<String>, val timetableUrlList:List<String>)