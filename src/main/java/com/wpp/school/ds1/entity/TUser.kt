package com.wpp.school.ds1.entity

import org.springframework.stereotype.Component
import javax.persistence.*

@Component
@Entity
@Table(name = "t_user", schema = "school", catalog = "")
open class TUser {
    @get:Id
    @get:Column(name = "id", nullable = false)
    open var id: Int = 0

    @get:Basic
    @get:Column(name = "user_name", nullable = false)
    open var userName: String = ""

    @get:Basic
    @get:Column(name = "password", nullable = false)
    open var password: String = ""

    @get:Basic
    @get:Column(name = "school_id", nullable = false)
    open var schoolId: String = ""

    @get:Basic
    @get:Column(name = "permissions", nullable = false)
    open var permissions: Int = 3

    @get:Basic
    @get:Column(name = "token", nullable = true)
    open var token: String? = null

    @get:Basic
    @get:Column(name = "status", nullable = false)
    open var status: Int = 1


    override fun toString(): String =
            "Entity of type: ${javaClass.name} ( " +
                    "id = $id " +
                    "userName = $userName " +
                    "password = $password " +
                    "schoolId = $schoolId " +
                    "permissions = $permissions " +
                    "token = $token " +
                    "status = $status " +
                    ")"

    // constant value returned to avoid entity inequality to itself before and after it's update/merge
    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as TUser

        if (id != other.id) return false
        if (userName != other.userName) return false
        if (password != other.password) return false
        if (schoolId != other.schoolId)return false
        if (permissions != other.permissions) return false
        if (token != other.token) return false
        if (status != other.status) return false

        return true
    }

}

