package com.wpp.school.ds1.entity

import org.springframework.stereotype.Component
import javax.persistence.*

@Component
@Entity
@Table(name = "t_show_img", schema = "school")
open class TShowImg {
    @get:Id
    @get:Column(name = "id", nullable = false)
    open var id: Int = 0

    @get:Basic
    @get:Column(name = "base_url", nullable = false)
    open var baseUrl: String = ""

    @get:Basic
    @get:Column(name = "detail_url", nullable = false)
    open var detailUrl: String = ""

    @get:Basic
    @get:Column(name = "module", nullable = false)
    open var module: Int? = null

    @get:Basic
    @get:Column(name = "status", nullable = false)
    open var status: Int = 1


    override fun toString(): String =
            "Entity of type: ${javaClass.name} ( " +
                    "id = $id " +
                    "baseUrl = $baseUrl " +
                    "detailUrl = $detailUrl " +
                    "module = $module " +
                    "status = $status " +
                    ")"

    // constant value returned to avoid entity inequality to itself before and after it's update/merge
    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as TShowImg

        if (id != other.id) return false
        if (baseUrl != other.baseUrl) return false
        if (detailUrl != other.detailUrl) return false
        if (module != other.module) return false
        if (status != other.status) return false

        return true
    }

}

