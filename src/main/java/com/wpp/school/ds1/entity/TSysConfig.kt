package com.wpp.school.ds1.entity

import org.springframework.stereotype.Component
import javax.persistence.*

@Component
@Entity
@Table(name = "t_sys_config", schema = "school")
open class TSysConfig {
    @get:Id
    @get:Column(name = "id", nullable = false)
    open var id: Int? = null

    @get:Basic
    @get:Column(name = "name", nullable = false)
    open var name: String? = null

    @get:Basic
    @get:Column(name = "config_main", nullable = false)
    open var configMain: String = ""

    @get:Basic
    @get:Column(name = "config_spare", nullable = true)
    open var configSpare: String? = null

    @get:Basic
    @get:Column(name = "module", nullable = false)
    open var module: Int? = null

    @get:Basic
    @get:Column(name = "status", nullable = false)
    open var status: Int = 1


    override fun toString(): String =
            "Entity of type: ${javaClass.name} ( " +
                    "id = $id " +
                    "name = $name " +
                    "configMain = $configMain " +
                    "configSpare = $configSpare " +
                    "module = $module " +
                    "status = $status " +
                    ")"

    // constant value returned to avoid entity inequality to itself before and after it's update/merge
    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as TSysConfig

        if (id != other.id) return false
        if (name != other.name) return false
        if (configMain != other.configMain) return false
        if (configSpare != other.configSpare) return false
        if (module != other.module) return false
        if (status != other.status) return false

        return true
    }

}

