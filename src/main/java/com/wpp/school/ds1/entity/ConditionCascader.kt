package com.wpp.school.ds1.entity

/**
 *
 * ClassName: ConditionCascader
 * Description:
 * date: 2022/4/17/0017 16:14
 * @author BOHE
 * @version
 * @since JDK 1.8
 */
data class ConditionCascader(val value: String, val label: String, val children: List<ConditionCascader>?)