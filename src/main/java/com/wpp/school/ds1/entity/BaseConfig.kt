package com.wpp.school.ds1.entity

/**
 *
 * ClassName: BaseConfig
 * Description:
 * date: 2022/4/6 13:50
 * @author BOHE
 * @version
 * @since JDK 1.8
 */
open class BaseConfig(val mainConfig: String, val spareConfig: String?, val id: Int?, val module: Int?) {
    constructor(mainConfig: String, spareConfig: String?) : this(mainConfig, spareConfig, null, null)
}