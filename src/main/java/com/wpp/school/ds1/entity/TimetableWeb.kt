package com.wpp.school.ds1.entity

/**
 *
 * ClassName: TimetableWeb
 * Description:
 * date: 2022/4/17/0017 22:41
 * @author BOHE
 * @version
 * @since JDK 1.8
 */
class TimetableWeb(val id: Int, val timetableUrl: String, val timetableUrlSelected: String,
                   val dayOfWeek: Int, val condition: String, val status: Int)