package com.wpp.school.ds1.entity

import org.springframework.stereotype.Component
import javax.persistence.*

@Component
@Entity
@Table(name = "t_condition", schema = "school")
open class TCondition {
    @get:Id
    @get:Column(name = "id", nullable = false)
    open var id: Int = 0

    @get:Basic
    @get:Column(name = "first_condition", nullable = false)
    open var firstCondition: String = ""

    @get:Basic
    @get:Column(name = "second_condition", nullable = false)
    open var secondCondition: String = ""

    @get:Basic
    @get:Column(name = "module", nullable = false)
    open var module: Int? = null

    @get:Basic
    @get:Column(name = "have_timetable", nullable = false)
    open var haveTimetable: Int? = 0

    @get:Basic
    @get:Column(name = "status", nullable = false)
    open var status: Int = 1


    override fun toString(): String =
            "Entity of type: ${javaClass.name} ( " +
                    "id = $id " +
                    "firstCondition = $firstCondition " +
                    "secondCondition = $secondCondition " +
                    "module = $module " +
                    "isHasTimetable = $haveTimetable " +
                    "status = $status" +
                    ")"

    // constant value returned to avoid entity inequality to itself before and after it's update/merge
    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as TCondition

        if (id != other.id) return false
        if (firstCondition != other.firstCondition) return false
        if (secondCondition != other.secondCondition) return false
        if (module != other.module) return false
        if (haveTimetable != other.haveTimetable) return false
        if (status != other.status) return false

        return true
    }

}

