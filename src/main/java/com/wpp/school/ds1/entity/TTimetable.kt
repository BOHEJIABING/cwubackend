package com.wpp.school.ds1.entity

import org.springframework.stereotype.Component
import javax.persistence.*

@Component
@Entity
@Table(name = "t_timetable", schema = "school", catalog = "")
open class TTimetable {
    @get:Id
    @get:Column(name = "id", nullable = false)
    open var id: Int = 0

    @get:Basic
    @get:Column(name = "timetable_url", nullable = false)
    open var timetableUrl: String = ""

    @get:Basic
    @get:Column(name = "timetable_url_selected", nullable = false)
    open var timetableUrlSelected: String = ""

    @get:Basic
    @get:Column(name = "day_of_week", nullable = false)
    open var dayOfWeek: Int = 1

    @get:Basic
    @get:Column(name = "status", nullable = false)
    open var status: Int = 1

    @get:Basic
    @get:Column(name = "condition_id", nullable = false)
    open var conditionId: Int = 0


    override fun toString(): String =
            "Entity of type: ${javaClass.name} ( " +
                    "id = $id " +
                    "timetableUrl = $timetableUrl " +
                    "timetableUrlSelected = $timetableUrlSelected " +
                    "dayOfWeek = $dayOfWeek " +
                    "status = $status " +
                    "conditionId = $conditionId " +
                    ")"

    // constant value returned to avoid entity inequality to itself before and after it's update/merge
    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as TTimetable

        if (id != other.id) return false
        if (timetableUrl != other.timetableUrl) return false
        if (timetableUrlSelected != other.timetableUrlSelected) return false
        if (dayOfWeek != other.dayOfWeek) return false
        if (status != other.status) return false
        if (conditionId != other.conditionId) return false

        return true
    }

}

