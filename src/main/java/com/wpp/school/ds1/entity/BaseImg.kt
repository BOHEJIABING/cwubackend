package com.wpp.school.ds1.entity

/**
 *
 * ClassName: BaseImg
 * Description:
 * date: 2022/4/3 0:05
 * @author BOHE
 * @version
 * @since JDK 1.8
 */
open class BaseImg(val url: String, val otherInfo: String) {

}