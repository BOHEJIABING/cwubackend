package com.wpp.school.ds1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wpp.school.ds1.entity.TUser;
import org.springframework.stereotype.Repository;

/**
 * ClassName: UserMapper
 * Description:
 * date: 2022/4/1 23:06
 *
 * @author BOHE
 * @since JDK 1.8
 */
@Repository
public interface UserMapper extends BaseMapper<TUser> {
}
