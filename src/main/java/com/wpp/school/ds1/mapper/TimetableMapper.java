package com.wpp.school.ds1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wpp.school.ds1.entity.TTimetable;
import org.springframework.stereotype.Repository;

/**
 * ClassName: TimetableMapper
 * Description:
 * date: 2022/4/1 23:07
 *
 * @author BOHE
 * @since JDK 1.8
 */
@Repository
public interface TimetableMapper extends BaseMapper<TTimetable> {
}
