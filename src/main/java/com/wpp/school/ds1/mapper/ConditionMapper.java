package com.wpp.school.ds1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wpp.school.ds1.entity.TCondition;
import org.springframework.stereotype.Repository;

/**
 * ClassName: ConditionMapper
 * Description:
 * date: 2022/4/1 0:23
 *
 * @author BOHE
 * @since JDK 1.8
 */
@Repository
public interface ConditionMapper extends BaseMapper<TCondition> {

    TCondition getConditionById(Integer conditionId);
}
