package com.wpp.school.ds1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wpp.school.ds1.entity.TShowImg;
import org.springframework.stereotype.Repository;

/**
 * ClassName: ShowImgMapper
 * Description:
 * date: 2022/4/1 23:07
 *
 * @author BOHE
 * @since JDK 1.8
 */
@Repository
public interface ShowImgMapper extends BaseMapper<TShowImg> {
}
