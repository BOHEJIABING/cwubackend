package com.wpp.school.ds1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wpp.school.ds1.entity.TSysConfig;
import org.springframework.stereotype.Repository;

/**
 * ClassName: SysConfigMapper
 * Description:
 * date: 2022/4/1 23:07
 *
 * @author BOHE
 * @since JDK 1.8
 */
@Repository
public interface SysConfigMapper extends BaseMapper<TSysConfig> {
}
