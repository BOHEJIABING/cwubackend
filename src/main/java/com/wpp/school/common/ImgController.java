package com.wpp.school.common;

import com.wpp.school.config.Json;
import com.wpp.school.config.Result;
import com.wpp.school.exception.PuyunException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping()
public class ImgController {

    @Value("${upload.picture.path}")
    private String uploadPicturePath;

    @Value("${mindcol.property.baseUrl}")
    protected String baseUrl;

    @javax.annotation.Resource
    private ResourceLoader resourceLoader;

    @Autowired
    private ServerConfig serverConfig;

    /**
     * 读取图片
     *
     * @param fileName
     * @return
     */
    @RequestMapping({"/img/{fileName:.+}", "/image/NoticeImg/{fileName:.+}", "/image/MusicImg/{fileName:.+}", "/image/RecommendImg/{fileName:.+}"})
    public ResponseEntity<Resource> show(@PathVariable String fileName) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "image/webp");
            return ok().headers(headers).body(resourceLoader.getResource("file:" + uploadPicturePath + fileName));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * 音频在线播放
     *
     * @param fileName
     * @return
     */
    @RequestMapping("/music/{fileName:.+}")
    public ResponseEntity<Resource> showMusic(@PathVariable String fileName) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "audio/mpeg");
            return ok().headers(headers).body(resourceLoader.getResource("file:" + uploadPicturePath + fileName));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * 视频在线播放
     */
    @RequestMapping("/video/{fileName:.+}")
    public ResponseEntity<Resource> showVideo(@PathVariable String fileName) {
        try {
            HttpHeaders headers = new HttpHeaders();
//            if (fileName.endsWith(".mkv"))
//                headers.add("Content-Type", "video/mkv");
            if (fileName.endsWith(".mp4"))
                headers.add("Content-Type", "video/mp4");
            return ok().headers(headers).body(resourceLoader.getResource("file:" + uploadPicturePath + fileName));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * 文件下载
     */
    @RequestMapping("/file/{fileName:.+}")
    public ResponseEntity<Resource> downFile(@PathVariable String fileName) {
        try {
            Resource file = resourceLoader.getResource("file:" + uploadPicturePath + fileName);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * 文件下载
     */
    @RequestMapping("/excel/{fileName:.+}")
    public ResponseEntity<Resource> downExcel(@PathVariable String fileName) {
//        try
//        {
//            HttpHeaders headers = new HttpHeaders();
//            headers.add("Content-Type","application/octet-stream");
//            return ResponseEntity.ok().headers(headers).body(resourceLoader.getResource("file:" + uploadPicturePath + fileName));
//        } catch (Exception e) {
//            return ResponseEntity.notFound().build();
//        }

        try {
            Resource file = resourceLoader.getResource("file:" + uploadPicturePath + fileName);
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/vnd.ms-excel");
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                    .headers(headers)
                    .body(file);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * 后端图片上传
     *
     * @param multipartFile
     * @return
     */
    @PostMapping("/upload")
    public Json upload(@RequestParam("file") MultipartFile multipartFile) {
        String fileName;
        try {
            //multipartFile.getOriginalFilename() 获取文件原始名称
            //new File(multipartFile.getOriginalFilename()) 根据获取到的原始文件名创建目标文件
            //multipartFile.transferTo() 将收到的文件传输到目标文件中
            fileName = RandomUtils.getUUID() + multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));
            multipartFile.transferTo(new File(fileName));
        } catch (IOException e) {
            e.printStackTrace();
            throw new PuyunException(Result.UPLOAD_FAIL);
        }
        return Json.rs("1", "上传成功", fileName);
    }

    /**
     * 富文本编辑器中的图片上传
     */
    @PostMapping("/uploadEdit")
    public Map uploadEdit(@RequestParam("file") MultipartFile multipartFile) {
        String fileName;
        try {
            fileName = RandomUtils.getUUID() + multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));
            multipartFile.transferTo(new File(fileName));
        } catch (IOException e) {
            e.printStackTrace();
            throw new PuyunException(Result.UPLOAD_FAIL);
        }
        Map<String, Object> map = new HashMap<>();
        Map<String, String> mapChild = new HashMap<>();
        mapChild.put("src", baseUrl + "/img/" + fileName);
        mapChild.put("title", "思政实践");
        mapChild.put("width", "100%");
        map.put("code", 0);
        map.put("msg", "图片上传成功");
        map.put("data", mapChild);
        return map;
    }

    /**
     * 前端图片上传
     *
     * @param multipartFile
     * @return
     */
    @PostMapping("/uploadF")
    public Json uploadF(@RequestParam("file") MultipartFile multipartFile) {
        String fileName = null;
        try {
            fileName = RandomUtils.getUUID() + multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));
            multipartFile.transferTo(new File(fileName));
        } catch (IOException e) {
            e.printStackTrace();
            throw new PuyunException(Result.UPLOAD_FAIL);
        }
        return Json.rs("上传成功", fileName, true);
    }

    /**
     * 前端图片上传
     *
     * @param
     * @return
     */
    @PostMapping("/uploadImage")
    public Json uploadImage(@RequestParam(value = "image", required = false) MultipartFile multipartFile0) {
        String fileName0 = null;
        String fileName1 = null;
        StringBuilder sb = new StringBuilder("");
        try {
            if (multipartFile0 != null) {
                fileName0 = RandomUtils.getUUID() + multipartFile0.getOriginalFilename().substring(multipartFile0.getOriginalFilename().lastIndexOf("."));
                multipartFile0.transferTo(new File(fileName0));
                sb.append(fileName0).append("?");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new PuyunException("上传失败");
        }
        return Json.rs("0", "上传成功", sb.toString());
    }

    /**
     * 前端图片上传
     *
     * @param
     * @return
     */
    @PostMapping("/uploadImageUniAPP")
    public Json uploadImage(@RequestParam(value = "image0", required = false) MultipartFile multipartFile0,
                            @RequestParam(value = "image1", required = false) MultipartFile multipartFile1,
                            @RequestParam(value = "image2", required = false) MultipartFile multipartFile2,
                            @RequestParam(value = "image3", required = false) MultipartFile multipartFile3,
                            @RequestParam(value = "image4", required = false) MultipartFile multipartFile4,
                            @RequestParam(value = "image5", required = false) MultipartFile multipartFile5) {
        String fileName0 = null;
        String fileName1 = null;
        String fileName2 = null;
        String fileName3 = null;
        String fileName4 = null;
        String fileName5 = null;
        StringBuilder sb = new StringBuilder("");
        try {
            if (multipartFile0 != null) {
                fileName0 = RandomUtils.getUUID() + multipartFile0.getOriginalFilename().substring(multipartFile0.getOriginalFilename().lastIndexOf("."));
                multipartFile0.transferTo(new File(fileName0));
                sb.append(fileName0).append("?");
            }
            if (multipartFile1 != null) {
                fileName1 = RandomUtils.getUUID() + multipartFile1.getOriginalFilename().substring(multipartFile1.getOriginalFilename().lastIndexOf("."));
                multipartFile1.transferTo(new File(fileName1));
                sb.append(fileName1).append("?");
            }
            if (multipartFile2 != null) {
                fileName2 = RandomUtils.getUUID() + multipartFile2.getOriginalFilename().substring(multipartFile2.getOriginalFilename().lastIndexOf("."));
                multipartFile2.transferTo(new File(fileName2));
                sb.append(fileName2).append("?");
            }
            if (multipartFile3 != null) {
                fileName3 = RandomUtils.getUUID() + multipartFile3.getOriginalFilename().substring(multipartFile3.getOriginalFilename().lastIndexOf("."));
                multipartFile3.transferTo(new File(fileName3));
                sb.append(fileName3).append("?");
            }
            if (multipartFile4 != null) {
                fileName4 = RandomUtils.getUUID() + multipartFile4.getOriginalFilename().substring(multipartFile4.getOriginalFilename().lastIndexOf("."));
                multipartFile4.transferTo(new File(fileName4));
                sb.append(fileName4).append("?");
            }
            if (multipartFile5 != null) {
                fileName5 = RandomUtils.getUUID() + multipartFile5.getOriginalFilename().substring(multipartFile5.getOriginalFilename().lastIndexOf("."));
                multipartFile5.transferTo(new File(fileName5));
                sb.append(fileName5).append("?");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new PuyunException("上传失败");
        }
        return Json.rs("1", "上传成功", sb.toString());
    }

}
