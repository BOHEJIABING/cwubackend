/**
 * FileName: RandomUtils
 * Author:   lvyabin
 * Date:     2019/2/6 10:11 PM
 * Description: 随机数工具类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.wpp.school.common;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Random;
import java.util.UUID;

/**
 * 〈随机数工具类〉
 *
 * @author lvyabin
 * @create 2019/2/6
 * @since 1.0.0
 */
public class RandomUtils {

    /**
     * 订单编号末位索引，为防止编号重复
     */
    public static Integer ORDER_NO_INDEX = 1000000;

    private RandomUtils() {

    }

    public static String orderNo() {
        //获取毫秒数
        Long milliSecond = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
        String orderNo;
        synchronized (ORDER_NO_INDEX) {
            orderNo = milliSecond+""+(++ORDER_NO_INDEX);
        }
        return orderNo;
    }

    /**
     * 获取随机数
     *
     * @param start 开始的值
     * @param end   结束的值
     * @return
     */
    public static int getRandom(int start, int end) {
        Random random = new Random();
        int result = random.nextInt(end - start) + start;
        return result;
    }

    /**
     * 获取随机数字内容的字符串
     *
     * @param start  开始位置
     * @param end    结束位置
     * @param length 共几位
     * @return
     */
    public static String getRandom(int start, int end, int length) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append(random.nextInt(end - start) + start);
        }
        return sb.toString();
    }

    /**
     * 产生随机字符串
     *
     * @param length
     * @return
     */
    public static String getRandomStr(int length) {
        String str = "zxcvbnmlkjhgfdsaqwertyuiopQWERTYUIOPASDFGHJKLZXCVBNM1234567890";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; ++i) {
            //产生0-61的数字
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }

    public static String getUUID() {
        String uuid = UUID.randomUUID().toString();//转化为String对象
        uuid = uuid.replace("-", "");//因为UUID本身为32位只是生成时多了“-”，所以将它们去点就可
        return uuid;
    }

    public static void main(String[] args) {
        System.out.println(getUUID());
    }

}
