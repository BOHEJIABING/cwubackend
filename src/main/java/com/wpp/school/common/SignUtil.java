package com.wpp.school.common;


import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * 测试httpclient 4.0 1. 重新设计了HttpClient 4.0 API架构，彻底从内部解决了所有 HttpClient 3.x 已知的架构缺陷代码。 2. HttpClient 4.0 提供了更简洁，更灵活，更明确的API。 3. HttpClient 4.0 引入了很多模块化的结构。 4.
 * HttpClient 4.0性能方面得到了不小的提升，包括更少的内存使用，通过使用HttpCore模块更高效完成HTTP传输。 5. 通过使用 协议拦截器(protocol interceptors), HttpClient 4.0实现了 交叉HTTP（cross-cutting HTTP protocol）
 * 协议 6. HttpClient 4.0增强了对连接的管理，更好的处理持久化连接，同时HttpClient 4.0还支持连接状态 7. HttpClient 4.0增加了插件式（可插拔的）的 重定向（redirect） 和 验证（authentication）处理。 8. HttpClient
 * 4.0支持通过代理发送请求，或者通过一组代理发送请求。 9. 更灵活的SSL context 自定义功能在HttpClient 4.0中得以实现。 10. HttpClient 4.0减少了在省城HTTP请求 和 解析HTTP响应 过程中的垃圾信息。 11. HttpClient团队鼓励所有的项目升级成
 * HttpClient 4.0
 *
 *
 */
public class SignUtil {

    /**
     * 将二进制转换成文件保存
     * @param instreams 二进制流
     * @param imgPath 图片的保存路径
     * @param imgName 图片的名称
     * @return
     *      1：保存正常
     *      0：保存失败
     */
    public static int saveToImgByInputStream(InputStream instreams, String imgPath, String imgName){
        int stateInt = 1;
        if(instreams != null){
            try {
                File file = new File(imgPath,imgName);//可以是任何图片格式.jpg,.png等
                File filePath = new File(imgPath);
                if (filePath.exists()) {
                    if (filePath.isDirectory()) {
                        System.out.println("dir exists");
                    } else {
                        System.out.println("the same name file exists, can not create dir");
                    }
                } else {
                    System.out.println("dir not exists, create it ...");
                    filePath.mkdir();
                }
                FileOutputStream fos=new FileOutputStream(file);
                byte[] b = new byte[1024];
                int nRead = 0;
                while ((nRead = instreams.read(b)) != -1) {
                    fos.write(b, 0, nRead);
                }
                fos.flush();
                fos.close();
            } catch (Exception e) {
                stateInt = 0;
                e.printStackTrace();
            } finally {
            }
        }
        return stateInt;//1 成功，0 失败
    }
}


