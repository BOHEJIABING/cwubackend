/**
 * FileName: FileUtils
 * Author:   lvyabin
 * Date:     2018/12/14 15:51
 * Description: 文件工具类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.wpp.school.common;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 〈文件工具类〉
 *
 * @author lvyabin
 * @create 2018/12/14
 * @since 1.0.0
 */
public class MD5Util {
    /**
     * 获取文件md5值
     */
    public static String getFileMD5(File file) throws Exception {
        StringBuffer sb = new StringBuffer("");
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(FileUtils.readFileToByteArray(file));
            byte b[] = md.digest();
            int d;
            for (int i = 0; i < b.length; i++) {
                d = b[i];
                if (d < 0) {
                    d = b[i] & 0xff;
                }
                if (d < 16) {
                    sb.append("0");
                }
                sb.append(Integer.toHexString(d));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new Exception("没有md5算法");
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception("文件有误");
        }
        return sb.toString();

    }

    /**
     * 获取字符串md5
     *
     * @param str
     * @return
     */
    public static String getStrMD5(String str) {
        String encodeStr = DigestUtils.md5Hex(str);
        return encodeStr;
    }

    /**
     * 获取字符串md5值
     * @param str
     * @param times
     * @param salt
     * @return
     */
    public static String getMD5Code(String str, int times, String salt) {
        String s = str+salt;
        for (int i = 0; i < times; i++) {
            s = DigestUtils.md5Hex(s);
        }
        return s;
    }

}
