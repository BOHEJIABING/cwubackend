package com.wpp.school.common

import com.wpp.school.ds1.entity.ConditionCascader
import com.wpp.school.ds1.entity.TCondition

/**
 *
 * ClassName: HandleList
 * Description:
 * date: 2022/4/17/0017 16:46
 * @author BOHE
 * @version
 * @since JDK 1.8
 */
class HandleCascader {
    companion object {
        fun handleList(list: List<TCondition>): MutableList<ConditionCascader> {

            val listCascader = ArrayList<ConditionCascader>()
            val stuList = list.filter { it.module == 40 }.map { it.firstCondition }.toSet().toMutableList()
            val teaList = list.filter { it.module == 41 }.map { it.firstCondition }.toSet().toMutableList()
            val roomList = list.filter { it.module == 42 }.map { it.firstCondition }.toSet().toMutableList()

            var stuSecondList = ArrayList<ConditionCascader>()
            val stuFirstList = ArrayList<ConditionCascader>()
            val teaFirstList = ArrayList<ConditionCascader>()
            var teaSecondList = ArrayList<ConditionCascader>()
            val roomFirstList = ArrayList<ConditionCascader>()
            var roomSecondList = ArrayList<ConditionCascader>()
            list.forEachIndexed { index, tCondition ->
                when (tCondition.module) {
                    40 -> {
                        stuList.forEachIndexed { _, sFirst ->
                            if (tCondition.firstCondition == sFirst) {
                                stuSecondList.add(ConditionCascader(tCondition.secondCondition, tCondition.secondCondition, null))
                                // 加条件，在最后一个二级ConditionCascader添加进list时，把list添加到firstList
                                if (list[index + 1].firstCondition != sFirst) {
                                    stuFirstList.add(ConditionCascader(sFirst, sFirst, stuSecondList))
                                    stuSecondList = ArrayList()
                                }
                            }
                        }
                    }
                    41 -> {
                        teaList.forEachIndexed { _, sFirst ->
                            if (tCondition.firstCondition == sFirst) {
                                teaSecondList.add(ConditionCascader(tCondition.secondCondition, tCondition.secondCondition, null))
                                if (list[index + 1].firstCondition != sFirst) {
                                    teaFirstList.add(ConditionCascader(sFirst, sFirst, teaSecondList))
                                    teaSecondList = ArrayList()
                                }
                            }
                        }
                    }
                    else -> {
                        roomList.forEachIndexed { _, sFirst ->
                            if (tCondition.firstCondition == sFirst) {
                                roomSecondList.add(ConditionCascader(tCondition.secondCondition, tCondition.secondCondition, null))
                                if ((index < list.size - 1 && list[index + 1].firstCondition != sFirst) || index == list.lastIndex) {
                                    roomFirstList.add(ConditionCascader(sFirst, sFirst, roomSecondList))
                                    roomSecondList = ArrayList()
                                }
                            }
                        }
                    }
                }

            }
            listCascader.add(ConditionCascader("学生", "学生", stuFirstList))
            listCascader.add(ConditionCascader("教师", "教师", teaFirstList))
            listCascader.add(ConditionCascader("教室", "教室", roomFirstList))
            return listCascader.toMutableList();
        }

    }
}