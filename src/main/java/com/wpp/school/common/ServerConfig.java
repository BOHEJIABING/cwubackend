package com.wpp.school.common;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Configuration
public class ServerConfig implements ApplicationListener<WebServerInitializedEvent> {
    /**
     * 获取访问的request中的host节点属性
     */
    private int serverPort;

    @Value("${upload.picture.head}")
    private String head;

    public String getUrl() {
        String ip = "";
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        ip = request.getHeader("host");
        return head+ip;//例如：123.12.12.12:8888
    }

    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
        this.serverPort = event.getWebServer().getPort();
    }
}
