package com.wpp.school.exception;

import com.wpp.school.common.JSONChange;
import com.wpp.school.config.Json;
import com.wpp.school.config.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.MessageFormat;
import java.util.Map;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @Value("${mindcol.property.packageName}")
    private String packageName;

    /**
     * 处理自定义的业务异常
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value = PuyunException.class)
    @ResponseBody
    public Json bizExceptionHandler(HttpServletRequest req, PuyunException e)  {
        String requestURI = req.getRequestURI(); //请求的uri;
        Map<String, String[]> parameterMap = req.getParameterMap();
        String parameter = null;
        try {
            parameter = JSONChange.objToJson(parameterMap).replace("\"", "");
        }catch (Exception ee){
            log.error("对象转Json出错！！！");
        }
        log.error("业务异常！{}",e.getMessage());
//        save2DB(requestURI,parameter,null,2,e.getMessage(),null);
        return Json.fail(e.getErrorCode(),e.getErrorMsg());
    }

//    /**
//     * 处理空指针的异常
//     * @param req
//     * @param e
//     * @return
//     */
//    @ExceptionHandler(value =NullPointerException.class)
//    @ResponseBody
//    public Json exceptionHandler(HttpServletRequest req, NullPointerException e){
//        logger.error("发生空指针异常！");
//        StringBuilder sbException = new StringBuilder();
//        for (StackTraceElement ele : e.getStackTrace()) {
//            String className = ele.getClassName();
//            String fileName = ele.getFileName();
//            if (!className.startsWith(packageName)||!fileName.endsWith(".java")) {
//                continue;
//            }
//            String methodName = ele.getMethodName();
//            int lineNumber = ele.getLineNumber();
//            sbException.append(MessageFormat.format("\tat {0}.{1}({2}:{3})\n",
//                    className, methodName, ele.getFileName(), lineNumber));;
//        }
//        logger.error(sbException.toString());
//        return Json.fail(Result.BODY_NOT_MATCH);
//    }


    /**
     * 处理其他异常
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value =Exception.class)
    @ResponseBody
    public Json exceptionHandler(HttpServletRequest req, Exception e){
        String requestURI = req.getRequestURI();//请求的uri;
        Map<String, String[]> parameterMap = req.getParameterMap();
        String parameter = null;
        try {
            parameter = JSONChange.objToJson(parameterMap).replace("\"", "");
        }catch (Exception ee){
            log.error("对象转Json出错！！！");
        }
        log.error("代码异常！{}",e.getClass().getName()+":"+e.getMessage());
        StringBuilder sbException = new StringBuilder();
        for (StackTraceElement ele : e.getStackTrace()) {
            String className = ele.getClassName();
            String fileName = ele.getFileName();
            if (!className.startsWith(packageName)||!fileName.endsWith(".java")) {
                continue;
            }
            String methodName = ele.getMethodName();
            int lineNumber = ele.getLineNumber();
            sbException.append(MessageFormat.format("\tat {0}.{1}({2}:{3})\n",
                    className, methodName, ele.getFileName(), lineNumber));
            //数据库处理
            log.error(sbException.toString());
//            save2DB(requestURI,parameter,lineNumber,1,e.getClass().getName()+":"+e.getMessage(),className);
            return Json.fail(Result.BODY_NOT_MATCH);
        }
        log.error(sbException.toString());
        return Json.fail(Result.BODY_NOT_MATCH);
    }

    /**
     *
     * @param uri  请求的url
     * @param param  参数
     * @param lineNumber  异常产生行数
     * @param type  1：Exception异常；2：PuyunException异常
     * @param message  异常原因
     * @param className 包名+类名
     */
//    private void save2DB(String uri,String param,Integer lineNumber,Integer type,String message,String className){
//        Pyexception exception = new Pyexception().setUri(uri).setParam(param).setLineNumber(lineNumber).setType(type).setMessage(message).setClassName(className);
//        exceptionService.save(exception);
//    }
}
